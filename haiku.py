def haiku_maker(x):
    x = x.replace('\n', ' ')
    y='ауоыиэяюёеaeiouy'
    count = 0
    t = x.split(' ')
    x = x.lower().split(' ')
    e = []
    q = 0
    for i in x:
        for j in i:
            if j in y:
                count+=1
        if count == 5 and len(e) == 0:
            e.append(' '.join(t[:(q+1)]).capitalize())
            count = 0
        elif count == 7 and len(e)>0:
            e.append(' '.join(t[len(e[0].split(' ')):(q+1)]).capitalize())
            count = 0
        elif count == 5 and len(e)>1:
            e.append(' '.join(t[len((e[0]+e[1]).split(' '))+1:(q+1)]).capitalize())
            count = 0
        q+=1
    return '\n'.join(e)
def count_vowels(x):
    x = x.replace('\n', ' ')
    result = 0
    count = 0
    y='ауоыиэяюёеaeiouy'
    for i in x.lower().split(' '):
        #print(i)
        for j in i:
            if j in y:
                #print(j)
                count+=1
        if result == 3 and count > 0:
            return False
        #print(count)
        if (count == 5 and not result) or (count == 7 and result == 1) or (count == 5 and result == 2):
            count = 0
            result += 1
        #print(count)
    if result == 3:
        return True
    else:
        return False
# print(count_vowels('Я вспомнил видос, где у мужика банка в жепе лопнула.'))
# print(haiku_maker('Я вспомнил видос, где у мужика банка в жепе лопнула.'))
# print(count_vowels('С яростным треском раскрываю полено внутри тишина'))
# print(haiku_maker('С яростным треском раскрываю полено внутри тишина'))
# print(count_vowels('An old silent pond... A frog jumps into the pond, splash! Silence again.'))

