import asyncio
from googletrans import Translator
from concurrent.futures import ThreadPoolExecutor
from functools import partial

_executor = ThreadPoolExecutor(10)


async def in_thread(func):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(_executor, func)


async def translate(text):
    try:
        result = await asyncio.gather(in_thread(partial(blocking_translate, text)))
    except:
        print("[-] Translation failed")
        return [text]

    return result


def blocking_translate(text):
    translator = Translator()
    translated = translator.translate(text, src="ru", dest="uk").text
    return translated
