import aioredis


async def log_user(user_id):
    conn = await aioredis.create_connection(("172.18.0.2", 6379))
    await conn.execute("set", user_id, "1")
    await conn.execute("expire", user_id, 10)


async def check_user(user_id):
    conn = await aioredis.create_connection(("172.18.0.2", 6379))
    res = await conn.execute("get", user_id)
    return res is None
