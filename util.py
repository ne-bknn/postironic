from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import io
import subprocess
import os
import json
import string
from translator import translate


def process_text(text, size):
    def split_chunk_to_lines(text, size):
        result = []
        while len(text) > 0:
            windex = max(text.rfind(s, 0, size + 1) for s in string.whitespace)
            if windex == -1 or len(text) < size:
                result.append(text[:size])
                text = text[size:]
            else:
                result.append(text[:windex])
                text = text[windex + 1 :]

        return "\n".join(result)

    return "\n".join(split_chunk_to_lines(s, size) for s in text.split("\n"))


def remove_prefix(text, obj):
    text = obj["regex"].sub("", text)
    print(f"*{text}*")
    return text


async def return_pic(text, obj):
    if obj["remove_prefix"]:
        text = remove_prefix(text, obj)

    text = (await translate(text))[0]
    text = process_text(text, obj["linelen"])
    img = Image.open(obj["picname"])
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(obj["font"], obj["fontsize"], encoding="UTF-8")

    print(f"[.] text: {text}")
    draw.text(tuple(obj["offset"]), text, tuple(obj["color"]), font=font)
    b = io.BytesIO()
    img.save(b, "JPEG")
    b.seek(0)
    return b


if __name__ == "__main__":
    print("я не могу снять джинсы")
