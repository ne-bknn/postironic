#!/usr/bin/env python3
import requests
import random
from concurrent.futures import ThreadPoolExecutor
import asyncio

_executor = ThreadPoolExecutor(5)

def get_govno():
    params = {  "access_token": "881e3218881e3218881e3218a4884208108881e881e3218d13a0664ce98c74aa65b25c4",
                "v": "5.126",
                "count": "0",
                "start_from": "0",
                "q": "#издач_говно"}
    out = requests.get("https://api.vk.com/method/newsfeed.search", params=params).json()["response"]
    count = out["total_count"]
    govnoID = random.randint(0, count-1)

    params["count"] = "1"
    params["start_from"] = str(govnoID)

    out = requests.get("https://api.vk.com/method/newsfeed.search", params=params).json()
    try:
        return out["response"]["items"][0]["text"]
    except:
        return "Коллеги, если будете хамить, то мы вас забаним. Без обид."

async def in_thread(func):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(_executor, func)

async def async_get_govno():
    try:
        result = await asyncio.gather(in_thread(get_govno))
    except:
        print("[-] Failed getting govno")

    return result[0]

