from mcstatus import MinecraftServer
import asyncio
from concurrent.futures import ThreadPoolExecutor

_executor = ThreadPoolExecutor(5)


async def in_thread(func):
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(_executor, func)


async def async_get_stats():
    try:
        result = await asyncio.gather(in_thread(get_stats))
    except:
        print("[-] Getting minecraft stats failed")
    try:
        return result[0]
    except UnboundLocalError:
        return ["да лежу я заебали паша гей"]


def get_stats():
    server = MinecraftServer.lookup("localhost:25565")

    query = server.query()
    playerlist = query.players.names
    version = query.software.version
    brand = query.software.brand
    return playerlist, version, brand
