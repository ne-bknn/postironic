import logging
from datetime import datetime
import asyncio
from aiogram import Bot, Dispatcher, executor, types, filters
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import re
from util import return_pic
import random
from telethon import TelegramClient
from redis_controller import log_user, check_user
import json
import os
import sys
from aiogram.types import ParseMode
import time
import aiohttp
import pandas as pd
import aiogram
import bs4
import haiku
from minetest import async_get_stats
from govno import async_get_govno
from telethon.tl.functions.messages import SearchRequest
from telethon.tl.types import InputMessagesFilterEmpty


API_TOKEN = os.environ["BOT_TOKEN"]
logging.basicConfig(level=logging.INFO)

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

with open("triggers.json", "r") as f:
    triggers = json.loads(f.read())

print("[+] Building regexes")
for obj in triggers:
    obj["regex"] = re.compile(obj["regex"], re.IGNORECASE)
print("[+] Building complete")

api_id = os.environ["API_ID"]
api_hash = os.environ["API_HASH"]

client = TelegramClient("session_name", api_id, api_hash)
client.start()


async def get_jumoreska():
    channel = await client.get_entity("myfavoritejumoreski")
    last = client.iter_messages(channel, limit=1)
    async for message in last:
        d = message.id
    n = random.randint(1, d)
    wanted = client.iter_messages(channel, limit=1, offset_id=n)
    async for message in wanted:
        return message.text


async def get_rust():
    channel = await client.get_entity("hacker_news_feed")
    ids = []
    async for msg in client.iter_messages(channel, search="rust"):
        ids.append(msg.id)

    wanted = client.iter_messages(channel, limit=1, ids=[random.choice(ids)])
    async for message in wanted:
        return message.text


async def get_nigger():
    channel = await client.get_entity("myfavoritejumoreski")
    ids = []
    async for msg in client.iter_messages(channel, search="негр"):
        print(msg.text)
        ids.append(msg.id)

    wanted = client.iter_messages(channel, limit=1, ids=[random.choice(ids)])
    async for message in wanted:
        return message.text


async def get_hn():
    channel = await client.get_entity("hacker_news_feed")
    last = client.iter_messages(channel, limit=1)
    async for message in last:
        d = message.id
    n = random.randint(1, d)
    wanted = client.iter_messages(channel, limit=1, offset_id=n)
    async for message in wanted:
        return message.text


async def get_crypto_price():
    url1 = f"https://www.coingecko.com/price_charts/12783/usd/24_hours.json"
    url2 = f"https://www.coingecko.com/price_charts/12783/rub/24_hours.json"
    url3 = f"https://www.coingecko.com/price_charts/1/usd/24_hours.json"

    async with aiohttp.ClientSession() as session:
        async with session.get(url1) as resp:
            data_usd = await resp.text()
        async with session.get(url2) as resp:
            data_rub = await resp.text()
        async with session.get(url3) as resp:
            data_btc = await resp.text()

    return json.loads(data_usd), json.loads(data_rub), json.loads(data_btc)


async def fetch_xkcd():
    url = "https://c.xkcd.com/random/comic/"
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            page = await resp.text()

    soup = bs4.BeautifulSoup(page, "lxml")
    link = soup.find("div", {"id": "comic"}).find("img")["src"]

    return "https:" + link


@dp.message_handler(commands=["xkcd"])
async def send_xkcd(message: types.Message):
    pic_link = await fetch_xkcd()
    await message.answer_photo(photo=pic_link)

@dp.message_handler(commands=["govno"])
async def send_govno(message: types.Message):
    succ = False
    counter = 0
    while not succ:
        pasta = await async_get_govno()
        try:
            await message.reply(pasta, reply_markup=create_delete_keyboard())
            succ = True
        except aiogram.utils.exceptions.MessageTextIsEmpty:
            await message.answer("Посос. Пробуем еще раз...")
            counter += 1
            if counter == 5:
                await message.answer("А, нет, не пробуем, пять раз проебались, иди дебаж")
                break
        except aiogram.utils.exceptions.MessageIsTooLong:
            await message.answer("Слишком длинное сообщение... Вечная проблема. Пробуем еще раз..")
            if counter == 5:
                await message.answer("А, нет, не пробуем, пять раз проебались, иди дебаж")
                break
            counter += 1

@dp.message_handler(commands=["die"])
async def sepukku(message: types.Message):
    if message.from_user["username"] == "pturtle":
        await message.reply("Inserting katana in my gut for you, sempai")
        await asyncio.sleep(10)
        await message.reply("SIKE! Poshel nahui")
    if message.from_user["username"] == "ne_bknn":
        await message.reply("Inserting katana in my gut for you, sempai")
        sys.exit()
    else:
        print("Someone requested sepukku")

@dp.message_handler(commands=["minecraft"])
async def send_minecraft_playerlist(message: types.Message):
    playerlist, version, brand = await async_get_stats()
    str_playerlist = "\n".join(playerlist)
    answer = f"ctf.sprush.rocks is running at {brand} version {version}\n\nIt has {len(playerlist)} player(s):\n\n" + str_playerlist
    await message.reply(answer)


@dp.message_handler(commands=["cyka"])
async def send_cyka(message: types.Message):
    await message.answer("https://www.youtube.com/watch?v=fCcegBWCeA0")

@dp.message_handler(commands=["crypto"])
async def send_ton_price(message: types.Message):
    print("[.] Ton price requested", message.from_user["username"])
    data_usd, data_rub, data_btc = await get_crypto_price()
    try:
        price_rub = data_rub["stats"][-1][1]
    except TypeError:
        print(data_rub)
        raise
    price_usd = round(data_usd["stats"][-1][1], 6)
    btc_price = data_btc["stats"][-1][1]
    usd_to_rub = price_rub / price_usd

    time_acquired = int(data_usd["stats"][-1][0] / 1000)
    dt = datetime.fromtimestamp(time_acquired).strftime("%Y-%m-%d %H:%M:%S")
    keyboard = create_delete_keyboard()
    amount_of_vodka = int((740*price_rub//1200))
    resp = await message.reply(
        f"{dt}\nBTC in USD: {btc_price}\nUSD in RUB: {usd_to_rub}\nTon price in USD: {price_usd}\n740 TON in RUB worth {int(740*price_rub)}\nOr {amount_of_vodka} bottles of Absolute Vodka 0.7\nOr {740*price_rub/21998} of Mansberg sofas",
        reply_markup=create_delete_keyboard(),
    )


def create_delete_keyboard():
    kb = InlineKeyboardMarkup(row_width=2)
    kb.add(InlineKeyboardButton("Delete Me", callback_data="delete_message"))
    return kb


@dp.callback_query_handler(lambda c: c.data and c.data == "delete_message")
async def handle_delete(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    original_message = callback_query.message["reply_to_message"]
    if callback_query.from_user["username"].lower() == "agalas":
        await callback_query.message.answer("Андрей иди нахуй")
    await cleanup(callback_query.message, original_message, t=0)


async def cleanup(*args, t=10):
    await asyncio.sleep(t)
    for message in args:
        try:
            await message.delete()
        except aiogram.utils.exceptions.MessageCantBeDeleted:
            k = await message.reply(
                "Cannot delete these message, please grant permissions"
            )
            await cleanup(k)


@dp.message_handler(commands=["jumoreska"])
async def send_jumoreska(message: types.Message):
    print("[.] Jumoreska requested by", message.from_user["username"])
    resp = await message.reply(await get_jumoreska())


@dp.message_handler(commands=["rust"])
async def send_rust(message: types.Message):
    print("[.] Rust requested by", message.from_user["username"])
    resp = await message.reply(await get_rust())


@dp.message_handler(commands=["nigger"])
async def send_rust(message: types.Message):
    print("[.] nigger requested by", message.from_user["username"])
    resp = await message.reply(await get_nigger())



@dp.message_handler(commands=["hn"])
async def send_hn(message: types.Message):
    print("[.] HN requested")
    await message.reply(await get_hn(), parse_mode=ParseMode.MARKDOWN)


@dp.message_handler(commands=["frontender"])
async def fronteder_game(message: types.Message):
    await asyncio.sleep(5)
    await message.answer(f"Ты фронтендер, @{message.from_user['username']}")


@dp.message_handler(commands=["trackers"])
async def get_trackers(message: types.Message):
    async with aiohttp.ClientSession() as session:
        async with session.get(
            "https://ngosang.github.io/trackerslist/trackers_best.txt"
        ) as resp:
            await message.reply(
                "```"
                + "\n".join(
                    [
                        line
                        for line in (await resp.read()).decode().split("\n")
                        if line.strip() != ""
                    ]
                )
                + "```",
                parse_mode=ParseMode.MARKDOWN,
            )


@dp.message_handler()
async def react(message: types.Message):
    username = message.from_user["username"];
    print("Message from ", username, ":", message.text)
    text = message.text.lower()
    await asyncio.sleep(random.random())
    if haiku.count_vowels(message.text):
        await message.answer(haiku.haiku_maker(message.text)+"\n\n"+f"- @{username}, 2021 Н.Э.")
    for obj in triggers:
        if obj["regex"].match(text) is not None:
            #if not await check_user(message.from_user.id):
            #    return

            #await log_user(message.from_user.id)
            resp = await message.answer_photo(await return_pic(text, obj))


if __name__ == "__main__":
    pd.set_option("display.float_format", lambda x: "%.5f" % x)
    executor.start_polling(dp, skip_updates=True)

